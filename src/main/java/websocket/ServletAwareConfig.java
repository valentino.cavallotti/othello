package websocket;

import jakarta.servlet.http.HttpSession;
import jakarta.websocket.HandshakeResponse;
import jakarta.websocket.server.*;

public class ServletAwareConfig extends ServerEndpointConfig.Configurator {
	@Override
    public void modifyHandshake(ServerEndpointConfig config, HandshakeRequest request, HandshakeResponse response) {
        HttpSession httpSession = (HttpSession) request.getHttpSession();
        config.getUserProperties().put("httpSessionId", httpSession.getId());
        String username = (String) httpSession.getAttribute("username");
        config.getUserProperties().put("username", username == null ? "" : username);
    }
}
