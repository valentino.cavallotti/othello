package websocket;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import com.google.gson.Gson;

import jakarta.websocket.*;
import jakarta.websocket.server.ServerEndpoint;
import model.Output;
import model.Room;
import persistence.UserPersistence;

@ServerEndpoint(value="/GameEndpoint", configurator=ServletAwareConfig.class)
public class GameEndpoint {
	
	private EndpointConfig config;
	
	static private Gson gson = new Gson();
	static private Map<String, Room> rooms = new HashMap<String, Room>();
	
	public GameEndpoint() {
		super();
	}
	
    @OnOpen
    public void onOpen(Session session, EndpointConfig config) throws IOException {
        this.config = config;
    }

    @OnMessage
    public void onMessage(String message, Session session) {
        System.out.println("Received message from " + session.getId() + ": " + message);
    	if (message.equals("host-request")) {
    		String roomCode = generateRoomCode();
    		
    		sendOutput(session, "hosted", roomCode);
    		
        	Room room = new Room(roomCode, null, null);
    		rooms.put(roomCode, room);
        	
    	}	
    	else if (message.startsWith("join-request")) { //joined:roomCode
    		String roomCode = message.split(":")[1];
    		if (rooms.containsKey(roomCode)) {
	    		Room room = rooms.get(roomCode);
	    		String httpSessionId = (String) config.getUserProperties().get("httpSessionId");
	    		String username = (String) config.getUserProperties().get("username");

	    		if (room.isThereNullSession()) {
	    			room.setNullSession(session, httpSessionId, username);
	    		}
	    		else if (room.isThereClosedSession() && room.hasHttpSessionId(httpSessionId)) { // caso di re-join
	    			room.setSessionFromHttpSessionId(session, httpSessionId);
	    			
	    			sendOutput(room.getOtherPlayerSession(session), "rejoin", "");
	    		} else {
	    			sendOutput(session, "error", "roomFull");
	    			return;
	    		}
	    		
	    		int playersJoined = room.bothPlayersJoined() ? 2 : 1;
	    		sendOutput(session, "players-joined", playersJoined);
	    		
    		} else {
    			sendOutput(session, "error", "noSuchRoom");
    		}
    	}
    	else if (message.startsWith("initialize-room")) {
    		String roomCode = message.split(":")[1];
    		Room room = rooms.get(roomCode);
    		sendOutput(session, "board", room.getBoard().getData());
    		sendOutput(session, "which-player", session.equals(room.getPlayer1Session()) ? 1 : 2);
    		
    		if (room.bothPlayersJoined()) {
    			if (room.getTurn() == 0) {
    				sendOutput(session, "opponent-username", room.getOpponentUsername(session));
    				sendOutput(room.getOtherPlayerSession(session), "opponent-username", room.getOpponentUsername(room.getOtherPlayerSession(session)));
    			}
	    		sendOutput(room.getCurrentPlayerSession(), "play", room.getTurn());
    		}
    	}
    	else if (message.startsWith("move-played")) { //played:roomCode:x:y
    		String roomCode = message.split(":")[1];
    		int moveX = Integer.parseInt(message.split(":")[2]);
    		int moveY = Integer.parseInt(message.split(":")[3]);
    		Room room = rooms.get(roomCode);
    		
    		if (room.getCurrentPlayerSession().equals(session)) {
    			if (room.getBoard().applyMove(moveX, moveY, room.getPlayerColor(session))) {
    				Session otherPlayer = room.getOtherPlayerSession(session);
    				
    				sendOutput(session, "board", room.getBoard().getData());
    				sendOutput(otherPlayer, "board", room.getBoard().getData());

    				if (room.getBoard().hasValidMoves(room.getPlayerColor(otherPlayer))) {
        				if (session.equals(room.getPlayer2Session())) {
        					room.incrementTurn();
        					sendOutput(session, "turn-update", room.getTurn());
        				}
    					
    					sendOutput(otherPlayer, "play", room.getTurn());
    					room.setCurrentPlayerSession(otherPlayer);
    					
    				} else if (room.getBoard().hasValidMoves(room.getPlayerColor(session))) {
        				if (session.equals(room.getPlayer1Session())) {
        					sendOutput(otherPlayer, "skip", room.getTurn());
        					room.incrementTurn();
        				} else if (session.equals(room.getPlayer2Session())) {
        					room.incrementTurn();
        					sendOutput(otherPlayer, "skip", room.getTurn());
        				}
        				sendOutput(session, "play", room.getTurn());
    				} else { // no valid moves for either side
    					room.setGameFinished(true);
    					updateUserStatistics(room);
    					sendOutput(session, "end", "");
    					sendOutput(otherPlayer, "end", "");
    				}				
    			} else {
    				sendOutput(session, "error", "invalidMove");
    			}
    		} else {
    			sendOutput(session, "error", "invalidPlayer");
    		}
    	} else if (message.startsWith("message")) {
    		String roomCode = message.split(":")[1];
    		String msg = message.substring(message.indexOf(roomCode) + roomCode.length() + 1);
    		
    		Room room = rooms.get(roomCode);
    		Session otherPlayer = room.getOtherPlayerSession(session);
    		
    		sendOutput(session, "own-message", msg);
    		sendOutput(otherPlayer, "message", msg);
    	}
    }
    
    @OnClose
    public void onClose(Session session) throws IOException {
    	for (Room r : rooms.values()) {
    		if (session.equals(r.getPlayer1Session()) || session.equals(r.getPlayer2Session())) {
    			Session otherPlayer = r.getOtherPlayerSession(session);
    			if (otherPlayer == null || !otherPlayer.isOpen()) {
    				System.out.println("Deleting room " + r.getRoomCode());
    				rooms.remove(r.getRoomCode());
        			break;
    			} else {
    				sendOutput(otherPlayer, "closed", "");
    				break;
    			}
    		}
    	}
    } 

    @OnError
    public void onError(Session session, Throwable throwable) {
        // Do error handling here
    }
    
    private String generateRoomCode() {
    	String[] characters = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890".split("");
    	Random r = new Random();
    	boolean isCodeUnique = false;
    	String code = "";
    	while (!isCodeUnique) {
    		StringBuilder sb = new StringBuilder();
    		for (int i = 0; i < 5; i++) {
    			sb.append(characters[r.nextInt(characters.length)]);
    		}
    		code = sb.toString();

    		isCodeUnique = true;
        	for (Room room : rooms.values()) {
        		if (room.getRoomCode().equals(code)) {
        			isCodeUnique = false;
        		}
        	}
    	}
    	return code;
    }
    
    private void updateUserStatistics(Room room) {
    	Session winnerSession;
    	if ((winnerSession = room.getWinnerSession()) != null) {
    		if (room.getPlayer1Username() != "")
    			UserPersistence.updateGameStatistics(room.getPlayer1Username(), winnerSession.equals(room.getPlayer1Session()));
    		if (room.getPlayer2Username() != "")
    			UserPersistence.updateGameStatistics(room.getPlayer2Username(), winnerSession.equals(room.getPlayer2Session()));
    	}
    }
    
    private void sendOutput(Session session, String type, Object data) {
    	if (session == null || !session.isOpen()) return;
    	
    	Output output = new Output(type, data);
    	try {
    		System.out.println("Sent output to " + session.getId() + ": [" + output.getType() + "] " + output.getContent());
			if (session.isOpen()) session.getBasicRemote().sendText(gson.toJson(output));
		} catch (IOException e) {
			e.printStackTrace();
		}
    }
}
