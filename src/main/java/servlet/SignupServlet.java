package servlet;

import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import model.AuthenticationBean;
import model.Output;
import model.User;
import persistence.UserPersistence;

import java.io.IOException;

import com.google.gson.Gson;

public class SignupServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private Gson gson = new Gson();
       
    public SignupServlet() {
        super();
    }

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		AuthenticationBean data = gson.fromJson(request.getReader(), AuthenticationBean.class);
		String username = data.getUsername();
		String password = data.getPassword();
		
		if (username != null && !username.isBlank() && password != null && !password.isBlank()) {
			User user = new User();
			user.setUsername(username);
			byte[] salt = HashManager.generateSalt();
			user.setSalt(HashManager.byteArrayToHex(salt));
			user.setHashedPassword(HashManager.hash(password, salt));
			user.setGamesWon(0);
			user.setGamesLost(0);
			if (UserPersistence.create(user)) {
				//System.out.println("signup OK");
				request.getSession().setAttribute("username", username);
				response.getWriter().write(gson.toJson(new Output("signed-up", null)));
			} else {
				response.getWriter().write(gson.toJson(new Output("error", "userAlreadyExists")));
			}
		}	
	}
}
