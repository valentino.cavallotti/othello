package servlet;

import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import model.Output;
import java.io.IOException;

import com.google.gson.Gson;

public class CheckLoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private Gson gson = new Gson();
	
    public CheckLoginServlet() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String username;
		if ((username = (String) request.getSession().getAttribute("username")) != null) {
			response.getWriter().write(gson.toJson(new Output("username", username)));
		} else 
			response.getWriter().write(gson.toJson(new Output("not-logged-in", null)));
	}
}

