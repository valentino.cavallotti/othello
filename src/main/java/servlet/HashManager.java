package servlet;

import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.KeySpec;

import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;

public class HashManager {
	public static byte[] generateSalt() {
		SecureRandom r = new SecureRandom();
		byte[] salt = new byte[16];
		r.nextBytes(salt);
		return salt;
	}
	
	public static String hash(String password, byte[] salt) {
		KeySpec spec = new PBEKeySpec(password.toCharArray(), salt, 65535, 128);
		SecretKeyFactory factory;
		
		try {
			factory = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA1");
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
			return null;
		}
		
		byte[] hash;
		
		try {
			hash = factory.generateSecret(spec).getEncoded();
		} catch (InvalidKeySpecException e) {
			e.printStackTrace();
			return null;
		}
		
		return byteArrayToHex(hash);
	}
	
	public static String byteArrayToHex(byte[] a) {
        StringBuilder sb = new StringBuilder(a.length * 2);
        for (byte b: a) {
            sb.append(String.format("%02x", b));
        }
        return sb.toString();
    }

    public static byte[] hexToByteArray(String h) {
        char[] chars = h.toCharArray();
        byte[] result = new byte[chars.length / 2];
        for (int i = 0; i < chars.length; i += 2) {
            result[i / 2] = Integer.decode("0x" + chars[i] + chars[i+1]).byteValue();
        }
        return result;
    }
}
