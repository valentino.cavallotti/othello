package servlet;

import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import model.AuthenticationBean;
import model.Output;
import persistence.UserPersistence;

import java.io.IOException;

import com.google.gson.Gson;

public class LoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private Gson gson = new Gson();
	
    public LoginServlet() {
        super();
    }

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		AuthenticationBean data = gson.fromJson(request.getReader(), AuthenticationBean.class);
		String username = data.getUsername();
		String password = data.getPassword();
		
		if (!UserPersistence.exists(username)) {
			response.getWriter().write(gson.toJson(new Output("error", "noSuchUser")));
			return;
		} else {
			String salt = UserPersistence.getSalt(username);
			
			if (username != null && password != null &&
				UserPersistence.checkPassword(username, HashManager.hash(password, HashManager.hexToByteArray(salt)))) {
				request.getSession().setAttribute("username", username);
				response.getWriter().write(gson.toJson(new Output("logged-in", null)));
			} else {
				response.getWriter().write(gson.toJson(new Output("error", "wrongPassword")));
			}
		}
	}
}

