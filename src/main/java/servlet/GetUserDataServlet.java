package servlet;

import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import model.User;
import persistence.UserPersistence;

import java.io.IOException;

import com.google.gson.Gson;

public class GetUserDataServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private Gson gson = new Gson();

    public GetUserDataServlet() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String username = (String) request.getSession().getAttribute("username");
		if (username == null) {
			response.getWriter().write(gson.toJson("ERROR"));
		} else {
			User user = UserPersistence.read(username);
			response.getWriter().write(gson.toJson(user));
		}
	} 
}
