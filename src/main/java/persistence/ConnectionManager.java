package persistence;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConnectionManager {
    public static Connection getConnection() {
        Connection conn = null;
        try {
        	try {
				Class.forName("org.sqlite.JDBC");
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
			}
        	
        	String path = ConnectionManager.class.getResource("/othello.db").getFile();
        	
            String url = "jdbc:sqlite:" + path;
            conn = DriverManager.getConnection(url);
            
            //System.out.println("Connessione a SQLite effettuata");
            
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        } 
        return conn;
    }
}
