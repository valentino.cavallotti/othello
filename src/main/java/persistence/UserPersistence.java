package persistence;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import model.User;

public class UserPersistence {
	
	public static boolean create(User user) {
		boolean result;
		String queryString = "insert into USERS values (?, ?, ?, ?, ?)";
		try (Connection conn = ConnectionManager.getConnection();
			PreparedStatement s = conn.prepareStatement(queryString)) {
			
			s.setString(1, user.getUsername());
			s.setString(2, user.getHashedPassword());
			s.setString(3, user.getSalt());
			s.setInt(4, user.getGamesWon());
			s.setInt(5, user.getGamesLost());
			s.executeUpdate();
			result = true;
		} catch (SQLException e) {
			result = false;
		}
		
		return result;
	}
	
	public static User read(String username) {
		User result = null;
		String queryString = "select * from USERS where username = ?";
		try (Connection conn = ConnectionManager.getConnection();
				PreparedStatement s = conn.prepareStatement(queryString)) {
			
			s.setString(1, username);
			ResultSet rs = s.executeQuery();
			result = new User();
			if (rs.next()) {
				result.setUsername(rs.getString("username"));
				result.setGamesWon(rs.getInt("gamesWon"));
				result.setGamesLost(rs.getInt("gamesLost"));
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return result;
	}
	
	public static boolean exists(String username) {
		boolean result = false;
		String queryString = "select * from USERS where username = ?";
		try (Connection conn = ConnectionManager.getConnection();
				PreparedStatement s = conn.prepareStatement(queryString)) {
				
			s.setString(1, username);
			ResultSet rs = s.executeQuery();
			if (rs.next()) {
				result = true;
			}
			
		} catch (SQLException e) {
			System.out.println("utente " + username + " non esistente");
		}
		
		return result;
	}
	
	public static boolean checkPassword(String username, String hashedPassword) {
		boolean result = false;
		String queryString = "select * from USERS where username = ? and hashedPassword = ?";
		try (Connection conn = ConnectionManager.getConnection();
				PreparedStatement s = conn.prepareStatement(queryString)) {
				
			s.setString(1, username);
			s.setString(2, hashedPassword);
			ResultSet rs = s.executeQuery();
			if (rs.next()) {
				result = true;
			}
			
		} catch (SQLException e) {
			System.out.println("utente " + username + " e relativa password non validi");
		}
		
		return result;
	}
	
	public static void updateGameStatistics(String username, boolean hasWon) {
		String columnToUpdate = hasWon ? "gamesWon" : "gamesLost";
		String queryString =
				"update USERS set " + columnToUpdate + " = " + columnToUpdate +  " + 1 "
				+ "where username = ?";
		try (Connection conn = ConnectionManager.getConnection();
				PreparedStatement s = conn.prepareStatement(queryString)) {
				
			s.setString(1, username);
			s.executeUpdate();
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public static String getSalt(String username) {
		String result = null;
		String queryString = "select salt from USERS where username = ?";
		try (Connection conn = ConnectionManager.getConnection();
				PreparedStatement s = conn.prepareStatement(queryString)) {
				
			s.setString(1, username);
			ResultSet rs = s.executeQuery();
			if (rs.next()) {
				result = rs.getString("salt");
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return result;
	}

}
