package model;

import java.util.Arrays;

public class Board {
	private String[][] data;
	public final static String player1Cell = "b";
	public final static String player2Cell = "w";
	public final static String emptyCell = "x";
	
	public Board() {
		data = new String[8][8];
		
		for (int y = 0; y < data.length; y++) {
			for (int x = 0; x < data[y].length;  x++) {
				if (y == 3 && x == 3 || y == 4 && x == 4) {
					data[y][x] = player1Cell;
				} else if (y == 4 && x == 3 || y == 3 && x == 4) {
					data[y][x] = player2Cell;
				} else { 
					data[y][x] = emptyCell;
				}
			}
		}
	}
	
	public boolean applyMove(int x, int y, String color) {
		boolean lineFound = false;
		if (isCellEmpty(x, y)) {
			for (int dirX = -1; dirX <= 1; dirX++) {
				for (int dirY = -1; dirY <= 1; dirY++) {
					if (dirX == 0 && dirY == 0) { continue; }
					
					boolean currentLineFound = false;
					boolean outOfBounds = false;
					int steps = 0;
					
					while (steps == 0 || steps > 0 && data[y + steps * dirY][x + steps * dirX].equals(getOtherColor(color))) {
						steps++;
						
						if (y + dirY * steps >= data.length || 
							y + dirY * steps < 0 ||
							x + dirX * steps >= data[0].length ||
							x + dirX * steps < 0) {
							outOfBounds = true;
							break;
						}						
					}
					if (outOfBounds) { continue; }
					
					if (steps > 1 && data[y + steps * dirY][x + steps * dirX].equals(color)) {
						lineFound = true;
						currentLineFound = true;
					}
					
					if (currentLineFound) {
						for (int step = 0; step < steps; step++) {
							data[y + step * dirY][x + step * dirX] = color;
						}
					}
				}
			}
		}
		return lineFound;
	}
	
	private boolean isCellEmpty(int x, int y) {
		return data[y][x].equals(emptyCell);
	}
	
	public boolean hasValidMoves(String color) {
		boolean lineFound = false;
		for (int y = 0; y < data.length && !lineFound; y++) {
			for (int x = 0; x < data[y].length && !lineFound; x++) {
				if (isCellEmpty(x, y)) {
					for (int dirX = -1; dirX <= 1 && !lineFound; dirX++) {
						for (int dirY = -1; dirY <= 1 && !lineFound; dirY++) {
							if (dirX == 0 && dirY == 0) { continue; }
							
							boolean outOfBounds = false;
							int steps = 0;
							
							while (steps == 0 || steps > 0 && data[y + steps * dirY][x + steps * dirX].equals(getOtherColor(color))) {
								steps++;
								
								if (y + dirY * steps >= data.length || 
									y + dirY * steps < 0 ||
									x + dirX * steps >= data[0].length ||
									x + dirX * steps < 0) {
									outOfBounds = true;
									break;
								}						
							}
							if (outOfBounds) { continue; }
							
							if (steps > 1 && data[y + steps * dirY][x + steps * dirX].equals(color)) {
								lineFound = true;
							}
						}
					}
				}
			}
		}
		return lineFound;
	}
	
	/*
	private boolean hasAdjacentEnemyPiece(int x, int y, String color) {
		boolean result = false;
		for (int offset_y = -1; offset_y <= 1 && !result; offset_y++) {
			for (int offset_x = -1; offset_x <= 1 && !result; offset_x++) {
				if ((offset_x == 0 && offset_y == 0) ||
					y + offset_y >= data.length ||
					y + offset_y < 0 ||
					x + offset_x >= data[0].length ||
					x + offset_x < 0) {
					continue;
				}
				if (data[y + offset_y][x + offset_x].equals(getOtherColor(color))) {
					result = true;
				}
			}
		}
		return result;
	}
	*/
	
	private String getOtherColor(String color) {
		if (color.equals(player2Cell)) 
			return player1Cell;
		else if (color.equals(player1Cell))
			return player2Cell;
		else return null;
	}
	
	public String getWinner() {
		String result = null;
		int[] playerCells = Arrays.stream(data) 
				.map((String[] row) -> {
					return new int[] { 
						(int) Arrays.stream(row).filter((String i) -> {return i.equals(player1Cell);}).count(),
						(int) Arrays.stream(row).filter((String i) -> {return i.equals(player2Cell);}).count() }; })
				.reduce(new int[] {0, 0}, (int[] totals, int[] amounts) -> { 
						totals[0] += amounts[0]; totals[1] += amounts[1]; return totals;});
		// [player1Cells, player2Cells]
		
		int player1Cells = playerCells[0];
		int player2Cells = playerCells[1];
		System.out.println("Player 1 cells: " + player1Cells);
		System.out.println("Player 2 cells: " + player2Cells);
		
		if (player1Cells > player2Cells) {
			return player1Cell;
		} else if (player1Cells < player2Cells) {
			return player2Cell;
		}
		return result;
	}
	
	public String[][] getData() {
		return this.data;
	}
}
