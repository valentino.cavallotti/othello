package model;

import jakarta.websocket.Session;

public class Room {
	private String roomCode;
	private Session player1Session;
	private Session player2Session;
	private String player1HttpSessionId;
	private String player2HttpSessionId;
	private String player1Username;
	private String player2Username;
	private Session currentPlayerSession;
	private boolean isGameFinished;
	private int turn;
	private Board board;
	
	public Room(String roomCode, Session player1Session, String player1HttpSessionId) {
		super();
		this.roomCode = roomCode;
		this.player1Session = player1Session;
		this.player1HttpSessionId = player1HttpSessionId;
		//this.currentPlayerSession = player1Session;
		this.isGameFinished = false;
		this.turn = 0;
		this.board = new Board();
	}
	
	public Session getOtherPlayerSession(Session somePlayer) {
		if (somePlayer.equals(player1Session))
			return player2Session;
		else if (somePlayer.equals(player2Session))
			return player1Session;
		else return null;
	}
	
	public boolean isThereNullSession() {
		return player1Session == null || player2Session == null;
	}
	
	public void setNullSession(Session session, String httpSessionId, String username) {
		if (player1Session == null) {
			player1Session = session;
			player1HttpSessionId = httpSessionId;
			player1Username = username;
			currentPlayerSession = session;
		} else if (player2Session == null) {
			player2Session = session;
			player2HttpSessionId = httpSessionId;
			player2Username = username;
		}
	}
	
	public boolean isThereClosedSession() {
		return !player1Session.isOpen() || !player2Session.isOpen();
	}
	
	public boolean hasHttpSessionId(String httpSessionId) {
		return player1HttpSessionId.equals(httpSessionId) || player2HttpSessionId.equals(httpSessionId);
	}
	
	public void setSessionFromHttpSessionId(Session session, String httpSessionId) {
		if (!player1Session.isOpen() && player1HttpSessionId.equals(httpSessionId)) {
			if (currentPlayerSession.equals(player1Session)) 
				currentPlayerSession = session;
			player1Session = session;
		}
		else if (!player2Session.isOpen() && player2HttpSessionId.equals(httpSessionId)) {
			if (currentPlayerSession.equals(player2Session)) 
				currentPlayerSession = session;
			player2Session = session;
		}
	}
	
	public Session getSessionFromHttpSessionId(String httpSessionId) {
		if (player1HttpSessionId.equals(httpSessionId))
			return player1Session;
		else if (player2HttpSessionId.equals(httpSessionId)) 
			return player2Session;
		else return null;
	}
	
	public boolean bothPlayersJoined() {
		return (player1Session != null && player1Session.isOpen() && player2Session != null && player2Session.isOpen());
	}
	
	public String getPlayerColor(Session somePlayer) {
		if (somePlayer.equals(player1Session))
			return Board.player1Cell;
		else if (somePlayer.equals(player2Session))
			return Board.player2Cell;
		else return null;
	}
	
	public void incrementTurn() {
		turn += 1;
	}
	
	public String getOpponentUsername(Session session) {
		String result = null;
		if (session.equals(player1Session)) {
			result = player2Username;
		} else if (session.equals(player2Session)) {
			result = player1Username;
		}
		return result;
	}

	public Session getWinnerSession() {
		Session result = null;
		String winner = board.getWinner();
		if (winner.equals(Board.player1Cell)) 
			result = player1Session;
		else if (winner.equals(Board.player2Cell)) 
			result =  player2Session;
		return result;
	}

	public String getRoomCode() {
		return roomCode;
	}

	public void setRoomCode(String roomCode) {
		this.roomCode = roomCode;
	}

	public Session getPlayer1Session() {
		return player1Session;
	}

	public void setPlayer1Session(Session player1Session) {
		this.player1Session = player1Session;
	}

	public Session getPlayer2Session() {
		return player2Session;
	}

	public void setPlayer2Session(Session player2Session) {
		this.player2Session = player2Session;
	}

	public String getPlayer1HttpSessionId() {
		return player1HttpSessionId;
	}

	public void setPlayer1HttpSessionId(String player1HttpSessionId) {
		this.player1HttpSessionId = player1HttpSessionId;
	}

	public String getPlayer2HttpSessionId() {
		return player2HttpSessionId;
	}

	public void setPlayer2HttpSessionId(String player2HttpSessionId) {
		this.player2HttpSessionId = player2HttpSessionId;
	}

	public Session getCurrentPlayerSession() {
		return currentPlayerSession;
	}

	public void setCurrentPlayerSession(Session currentPlayerSession) {
		this.currentPlayerSession = currentPlayerSession;
	}

	public boolean isGameFinished() {
		return isGameFinished;
	}

	public void setGameFinished(boolean isGameFinished) {
		this.isGameFinished = isGameFinished;
	}

	public int getTurn() {
		return turn;
	}

	public void setTurn(int turn) {
		this.turn = turn;
	}

	public Board getBoard() {
		return board;
	}

	public void setBoard(Board board) {
		this.board = board;
	}

	public String getPlayer1Username() {
		return player1Username;
	}

	public void setPlayer1Username(String player1Username) {
		this.player1Username = player1Username;
	}

	public String getPlayer2Username() {
		return player2Username;
	}

	public void setPlayer2Username(String player2Username) {
		this.player2Username = player2Username;
	}
}
