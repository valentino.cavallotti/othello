
export let room = {
	is_host: false,
	has_someone_joined: false,
	can_play: false,
	skipped_turn: false,
	game_ended: false,
	other_player_disconnected: false,
	new_message: false,
	
	socket: undefined,
	room_code: "",
	opponent_username: "",
	turn: 0,
	
	reset: function() {
		this.is_host = false
		this.has_someone_joined = false
		this.can_play = false
		this.skipped_turn = false
		this.game_ended = false
		this.other_player_disconnected = false
		this.chat_messages = []
		this.board.forEach((row) => {row.fill(this.empty_cell)})
		this.turn = 0
	},
	
	player_1_cell: "b",
	player_2_cell: "w",
	empty_cell: "x",
	board: new Array(8).fill(0).map(() => new Array(8).fill(0)),
	get_cell_amount: function(cell_type) {
		return this.board.map((row) => { return row.filter((i) => { return i === cell_type }).length })
			.reduce((total, amount) => { return total + amount }, 0)
	},
	get_white_score: function() { return this.get_cell_amount(this.player_2_cell) },
	get_black_score: function() { return this.get_cell_amount(this.player_1_cell) },
	get_empty_cells: function() { return this.get_cell_amount(this.empty_cell) },
	
	chat_messages: []
}