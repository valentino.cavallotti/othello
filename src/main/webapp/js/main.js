import {Home} from "./components/home.js"		
import {Rules} from "./components/rules.js"
import {Game} from "./components/game.js"
import {Signup} from "./components/signup.js"
import {Login} from "./components/login.js"
import {Profile} from "./components/profile.js"
import {room} from "./model/room.js"
import {auth} from "./model/auth.js"

m.request({
	method: "GET",
	url: "/CheckLoginServlet"
}).then(function(result) {
	console.log("checkLogin: received " + result.type + ", " + result.content)
	if (result.type === "username") {
		auth.is_logged_in = true
		auth.username = result.content
	} else if (result.type === "not-logged-in") {
		console.log("user not logged in")
	}
})

var gameRouteResolver = {
	onmatch: function(args) {
		if (args.type === "host") {
			return new Promise(
				function(resolve, reject) {
					room.socket = new WebSocket("ws://" + window.location.host + "/GameEndpoint")
					room.socket.onopen = function() { room.socket.send("host-request") }
					room.socket.onmessage = function(event) { 
						var data = JSON.parse(event.data)
						console.log("(host) received " + data.type + " " + data.content);
						if (data.type === "hosted") {
							room.room_code = data.content
							m.route.set("/game/" + room.room_code)
						}
					}
				}
			)	
		} else if (args.type !== "") {
			return new Promise(
				function(resolve, reject) {
					let on_message = function(event) { 
						var data = JSON.parse(event.data)
						console.log("(join) received " + data.type + " " + data.content);
						if (data.type === "players-joined") {
							if (data.content === 2) 
								room.has_someone_joined = true
							room.room_code = args.type
							resolve(Game)
						} else if (data.type === "error") {
							var error = data.content
							console.log(error)
							if (error === "noSuchRoom") {
								m.route.set("/home")
							}
						}
					}
					
					if (room.socket === undefined || room.socket.readyState == 3) {
						room.socket = new WebSocket("ws://" + window.location.host + "/GameEndpoint")
						room.socket.onmessage = on_message
						room.socket.onopen = function() { room.socket.send("join-request:" + args.type) }
					} else {
						room.socket.onmessage = on_message
						room.socket.send("join-request:" + args.type)
					}
				}
			)
		} else {
			return Home
		}
	}
}

m.route(document.body, "/home", {
	"/home": Home,
	"/rules": Rules,
	"/game/:type": gameRouteResolver,
	"/signup": Signup,
	"/login": Login,
	"/profile": Profile
})