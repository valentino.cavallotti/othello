export var SiteHeader = {
	view: function() {
		return m("div.header", [
			m("img", {src: "/resources/logo.svg"}),
			m("h1", "Othello"),
		])
	}
}