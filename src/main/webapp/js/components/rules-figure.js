export var RulesFigure = {
	view: function(vnode) {
		return m("figure", {style: {float: vnode.attrs.float, textAlign: "center"} }, [
			...vnode.attrs.images.map(function(src) { return m("img.rules-img", {src: src}) }),
			m("figcaption.rules-caption", {}, vnode.attrs.caption)	 
		])
	}
}