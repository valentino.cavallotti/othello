import {SiteHeader} from "./site-header.js"
import {auth} from "../model/auth.js"

export function Home() {
	var current_join_code = ""
	
	var join_room = function(e) {
		if (current_join_code !== "") {
			m.route.set("/game/" + current_join_code.toUpperCase().trim())
		}
		e.preventDefault();
		return false;
	}
	
	var logout = function() {
		m.request({
			method: "GET",
			url: "/LogoutServlet"
		}).then(function(result) {
			console.log(result)
			auth.is_logged_in = false
			auth.username = ""
		})
	}
	
	return {
		view: function() {
			return m("div.home-container", [
				m(SiteHeader),
				m("ul.top-links", [
					m("li", [
						m(m.route.Link, {class: "home-link", href: "/rules"}, "Regole"),
					]),
					m("li", [
						m(m.route.Link, {class: "home-link", href: "/signup",style: {display: auth.is_logged_in ? "none" : "initial"}}, "Registrati"),		
					]),
					m("li", [
						m(m.route.Link, {class: "home-link", href: "/login", style: {display: auth.is_logged_in ? "none" : "initial"}}, "Login"),
					]),
					m("li", [
						m(m.route.Link, {class: "home-link", href: "/profile", style: {display: auth.is_logged_in ? "initial" : "none"}}, "Profilo"),
					]),
					m("li", [
						m("a", {class: "home-link", href: "#", onclick: logout, style: {display: auth.is_logged_in ? "initial" : "none"}}, "Log out"),
					]),		
				]),
				
				m("span", [
					m(m.route.Link, {href: "/game/host"}, "Crea una nuova stanza"),
					",",
					m("br"),
					"oppure entra in una stanza esistente:"
				]),
				
				m("form.home-form", {onsubmit: (e) => {join_room(e)}}, [
					m("input", {type: "text", placeholder: "Codice stanza", 
						oninput: function (e) {current_join_code = e.target.value}}),
					m("input", {type: "submit", value: "Entra"})
				]),
				
				m("p.logged-in-message", {style: {display: auth.is_logged_in ? "initial" : "none"}}, "Logged in as " + auth.username)		
			])
		}
	}
}