export function GameStatus() {
	
	let getStatusString = function(room) {
		if (!room.has_someone_joined) 
			return "In attesa dell'altro giocatore..."
		else if (room.game_ended) {
			let white_score = room.get_white_score(), black_score = room.get_black_score()
			if (white_score === black_score) return "Pareggio"
			else if ((white_score > black_score && room.is_host) || (black_score > white_score && !room.is_host)) 
				return "Hai perso..."
			else 
				return "Hai vinto!!!"
		}
		else if (room.other_player_disconnected)
			return "L'altro giocatore si è disconnesso"
		else if (room.skipped_turn)
			return "Nessuna mossa disponibile: il turno passa all'altro giocatore"
		else if (room.can_play)
			return "Turno " + room.turn + " - Tuo turno"
		else
			return "Turno " + room.turn + " - Turno dell'avversario"
	}
	
	return {
		view: function(vnode) {
			return m("span", {}, getStatusString(vnode.attrs.room))
		}
	}
}