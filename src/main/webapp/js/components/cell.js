function select_cell(room, x, y) {
	if (is_move_valid(room, x, y)) {
		room.socket.send("move-played:" + room.room_code + ":" + x + ":" + y)
		room.can_play = false
		console.log("mossa valida: " + x + ", " + y)
	} else {
		console.log("mossa non valida: " + x + ", " + y)
	}
}

function is_move_valid(room, x, y) {
	if (room.board[y][x] != room.empty_cell) { return false }
	
	let own_cell, opponent_cell
	if (room.is_host) {
		own_cell = room.player_1_cell
		opponent_cell = room.player_2_cell
	} else {
		own_cell = room.player_2_cell
		opponent_cell = room.player_1_cell
	}
	
	let line_found = false
	for (let dir_y = -1; dir_y <= 1 && !line_found; dir_y++) {
		for (let dir_x = -1; dir_x <= 1 && !line_found; dir_x++) {
			if (dir_x == 0 && dir_y == 0) continue;
			let steps = 1
			
			while (room.board[y + dir_y * steps] != undefined && 
				room.board[y + dir_y * steps][x + dir_x * steps] == opponent_cell) {
					
				steps++
			}
			if (steps > 1 &&  room.board[y + dir_y * steps] != undefined 
				&& room.board[y + dir_y * steps][x + dir_x * steps] == own_cell) {
				line_found = true
			}
		}
	}
	if (line_found) { return true }	
}

export var Cell = {
	view: function(vnode) {
		return m("div.cell", {
			onclick: () => { 
				if (vnode.attrs.room.can_play && vnode.attrs.room.board[vnode.attrs.y][vnode.attrs.x] === vnode.attrs.room.empty_cell)
					select_cell(vnode.attrs.room, vnode.attrs.x, vnode.attrs.y)},
		}, [
			vnode.attrs.room.board[vnode.attrs.y][vnode.attrs.x] === vnode.attrs.room.player_2_cell ?
				m("img.game-piece-svg", {src: "/resources/white_circle.svg"}) :
			(vnode.attrs.room.board[vnode.attrs.y][vnode.attrs.x] === vnode.attrs.room.player_1_cell ?
				m("img.game-piece-svg", {src: "/resources/black_circle.svg"}) : null)
		])
	}
}