import {SiteHeaderTiny} from "./site-header-tiny.js"
import {Grid} from "./grid.js"
import {Chat} from "./chat.js"
import {GameStatus} from "./game-status.js"
import {room} from "../model/room.js"
import {auth} from "../model/auth.js"

//import {auth} from "../model/auth.js"

let get_message_time = function() {
	let now = new Date()
	return "[" + now.getHours() + ":" + ("0" + now.getMinutes()).slice(-2) + ":" + ("0" + now.getSeconds()).slice(-2) + "]"
}

let on_message = function(event) {
	var data = JSON.parse(event.data)
	console.log("received: [" + data.type + "] " + data.content);
	switch(data.type) {
		case "which-player":
			if (data.content === 1)
				room.is_host = true
			break
		case "opponent-username":
			room.opponent_username = data.content === "" ? "Giocatore anonimo" : data.content
			break
		case "board":
			room.board = data.content
			break
		case "play":
			room.turn = data.content
			room.skipped_turn = false
			room.can_play = true
			if (room.is_host && room.turn == 0) {
				room.has_someone_joined = true
			}
			break
		case "turn-update":
			room.turn = data.content
			break
		case "skip":
			room.turn = data.content
			room.skipped_turn = true
			break
		case "end":
			room.game_ended = true
			break
		case "own-message":
			let own_message = data.content
			room.new_message = true
			room.chat_messages.push({type: "own", text: own_message, timestamp: get_message_time()});
			break
		case "message":
			let message = data.content
			room.new_message = true
			room.chat_messages.push({type: "other-player", text: message, timestamp: get_message_time()});
			break
		case "closed":
			room.other_player_disconnected = true
			break
		case "rejoin":
			room.other_player_disconnected = false;
			break
		case "error":
			let error = data.content
			console.log(error)
			if (error === "invalidMove") {
				room.can_play = true
			}
			break
		default:
			console.log("unexpected message: " + data)
	}
	m.redraw()
}

export var Game = {
	oninit: function(vnode) {
		room.socket.onmessage = on_message
		room.socket.send("initialize-room:" + room.room_code)
	},
	onremove: function() {
		room.socket.close()
		room.reset()
	},
	view: function() {
		return m("div.game-container", [
			m(SiteHeaderTiny),
			m(m.route.Link, {href: "/home"}, "Torna alla Home"),
			
			m("h3.game-title", "Pedine: " + (room.is_host ? "nere" : "bianche") + 
				" | Avversario: " + (room.opponent_username === "" ? "In attesa..." : room.opponent_username)),
			m("span", {}, "Codice stanza: " + room.room_code + " | "),
			
			m(GameStatus, {room: room}),
			
			m("div.grid-and-chat", [
				m(Grid, {room: room}),
				m(Chat, {room: room})
			]),
			
			m("p.logged-in-message", {style: {display: auth.is_logged_in ? "initial" : "none"}}, "Logged in as " + auth.username)		
		])
	}
}