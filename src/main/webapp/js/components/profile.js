import {auth} from "../model/auth.js"

export function Profile() {
	
	var username = "", gamesWon = 0, gamesLost = 0
	
	return {
		oninit: function() {
			m.request({
				method: "GET",
				url: "/GetUserDataServlet"
			}).then(function(result) {
				console.log("profile result: " + result)
				username = auth.username
				gamesWon = result.gamesWon
				gamesLost = result.gamesLost
			})
		},
		view: function() {
			return m("div.profile-container", [
				m("div.header-tiny", [
					m("img", {src: "/resources/logo.svg"}),
					m("h2", "Othello"),
				]),
				m(m.route.Link, {href: "/home"}, "Torna alla Home"),
				m("h1", username),
				m("p", "Partite vinte: " + gamesWon),
				m("p", "Partite perse: " + gamesLost)
			])
		}
	}
	
}