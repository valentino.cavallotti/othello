export var SiteHeaderTiny = {
	view: function() {
		return m("div.header-tiny", [
			m("img", {src: "/resources/logo.svg"}),
			m("h2", "Othello"),
		])
	}
}