import {SiteHeaderTiny} from "./site-header-tiny.js"
import {RulesFigure} from "./rules-figure.js"

export var Rules = {
	view: function() {
		return m("div.rules-container", [
			m(SiteHeaderTiny),
			m(m.route.Link, {href: "/home"}, "Torna alla Home"),
			m("h1", "Regole"),

			m(RulesFigure, {float: "right", caption: "Figura 1: disposizione iniziale", images: [
				"./resources/starting_position.png"
		 	]}),
			m("p", 
				`Il gioco di Othello richiede due giocatori. È giocato su
				un tavolo da gioco con 64 celle, otto per lato, alle cui colonne sono assegnate
				lettere dalla a alla h, e alle cui righe sono assegnati i numeri da 1 a 8.
				Ad uno dei giocatori è assegnato il colore nero, all'altro il colore bianco; 
				Il giocatore con le pedine nere inizia a giocare per primo.
				Il gioco inizia con una disposizione del tavolo come mostrato nella figura 1.`
			),
			
			m("p", 
				`Ad ogni turno, ogni giocatore deve giocare una mossa valida, se possibile.
				Si definisce valida una mossa tale che catturi pedine dell'avversario in
				almeno una direzione – orizzontale, verticale e/o diagonale, per un totale
				di otto direzioni possibili.`
			),
			
			m(RulesFigure, {float: "left", caption: "Figura 2: semplice situazione di cattura", images: [
				"./resources/simple_capture_1.png",
				"./resources/simple_capture_2.png"
		 	]}),

			m("p", 
				`La cattura di pedine si ha quando, una volta piazzata una nuova pedina,
				 esiste un'altra pedina del proprio colore tale che tra le due è presente
				 una fila ininterrotta di una o più pedine dell'avversario. Tutte le 
				 pedine avversarie in questione si girano, diventando di proprietà dell'
				 altro giocatore; il turno passa poi all'avversario.`
			),
			
			m("p", 
				`Il piazzamento di una pedina provoca l'esecuzione di tutte le catture possibili
				nelle otto direzioni – si veda il caso di cattura multipla in figura 3.`
			),
			
			m(RulesFigure, {float: "right", caption: "Figure 3: Cattura multipla in più direzioni", images: [
				"./resources/multiple_capture_1.png",
				"./resources/multiple_capture_2.png"
		 	]}),
		 	
			m("p", {style: {clear: "left"}}, 
				`Se un giocatore non ha mosse valide, cioè se non può piazzare alcuna pedina
				tale da generare una cattura, il turno passa all'avversario.
				Se entrambi i giocatori non hanno mosse valide, ovvero se il tavolo da gioco
				si riempie, o se tutte le pedine diventano dello stesso colore, o in altre 
				situazioni in cui nessuno dei giocatori può generare catture, la partita termina.`
			),
			m("p", 
				`Vince la partita il giocatore che abbia più pedine del proprio colore
				sul tavolo. In caso di uguale numero di pedine nere e bianche a fine partita,
				il risultato è un pareggio.`),
				
			m(m.route.Link, {href: "/home"}, "Torna alla Home")
		])
	}
}