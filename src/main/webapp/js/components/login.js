import {SiteHeaderTiny} from "./site-header-tiny.js"
import {auth} from "../model/auth.js"

export function Login() {
	var errorMessage = ""
	
	var login = function(e) {
		m.request({
			method: "POST",
			url: "/LoginServlet",
			body: {username: currentUsername, password: currentPassword}
		}).then(function(result) {
			if (result.type === "logged-in") {
				auth.is_logged_in = true
				auth.username = currentUsername
				m.route.set("/home")
			} else if (result.type === "error") {
				if (result.content === "noSuchUser")
					errorMessage = "Username non esistente"
				else if (result.content === "wrongPassword")
					errorMessage = "Password non corretta"
				setTimeout((function() { errorMessage = ""; m.redraw() }), 6000)
			}
		})
	
		e.preventDefault()
		return false
	}
	
	var currentUsername = "", currentPassword = ""
	
	return { 
		view: function() {
			return m("div.login-container", [
				m(SiteHeaderTiny),
				m(m.route.Link, {href: "/home"}, "Torna alla Home"),
				m("h2.login-title", "Accedi"),
				m("form", {onsubmit: (e) => { login(e) }}, [
					m("input[type='text'][placeholder='username']",
						{oninput: (e) => {currentUsername = e.target.value}}),
					m("br"),
					m("input[type='password'][placeholder='password']",
						{oninput: (e) => {currentPassword = e.target.value}}),
					m("br"),
					m("input[type='submit'][value='Accedi'].login-button")
				]),
				m("p.error-message", errorMessage)
			])
		}
	}
}