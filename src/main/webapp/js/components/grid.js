import {Cell} from "./cell.js"

export var Grid = {
	view: function(vnode) {
		return m("div.layout-grid", [
			m("div.letters-top-grid", [
				...Array.from("abcdefgh").map((letter) => {return m("div", letter)})
			]),
			m("div.numbers-left-grid", [
				...Array.from("12345678").map((number) => {return m("div.numbers-left-cell", number)})
			]),
			m("div.board-grid", [
				...Array.from(Array(64).keys())
						.map((i) => {
							return m(Cell, {
								x: i % 8,
								y: Math.floor(i / 8),
								room: vnode.attrs.room})
						})
			]),
			m("p.piece-amounts", 
				"Bianche: " + vnode.attrs.room.get_white_score() + 
				", Nere: " + vnode.attrs.room.get_black_score() + ", " +
				vnode.attrs.room.get_empty_cells() + " celle vuote"),		
		])
	}
}