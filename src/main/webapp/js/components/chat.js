import {auth} from "../model/auth.js"

export function Chat(initialVnode) {
	var currentTypedMessage
	
	let sendMessage = function(event, room) {
		if (currentTypedMessage !== "") {
			room.socket.send(
				"message:" + room.room_code + ":" + currentTypedMessage)
			currentTypedMessage = ""
		}
		event.preventDefault()
		return false
	}
	
	return {
		onupdate: function(vnode) {
			if (vnode.attrs.room.new_message) {
				let chat_display_container = vnode.dom.firstChild
				chat_display_container.scrollTop = chat_display_container.scrollHeight
				vnode.attrs.room.new_message = false
			}
		},
		view: function(vnode) {
			return m("div.chat", [
				m("div.chat-display-container", [
					m("table.chat-display", [
						...Array.from(Array(vnode.attrs.room.chat_messages.length).keys())
							.map((i) => {
								let message = vnode.attrs.room.chat_messages[i]
								if (message.type === "own")
									return m("tr.chat-own-displayed-message", [
										m("td.chat-message-timestamp.message-info", message.timestamp),
										m("td.chat-message-text", [
											m("span.message-info", auth.is_logged_in ? auth.username : "Giocatore anonimo (tu)"),
											": " + message.text
										])
									])
								else if (message.type === "other-player") {
									return m("tr.chat-other-displayed-message", [
										m("td.chat-message-timestamp.message-info",  message.timestamp),
										m("td.chat-message-text", [
											m("span.message-info", vnode.attrs.room.opponent_username),
											": " + message.text
										])
									])
								}
							})
					])
				]),
				m("form.chat-form", {onsubmit: (event) => {sendMessage(event, vnode.attrs.room)}}, [
					m("input.chat-input[type='text'][placeholder='Scrivi un messaggio'][maxlength='255']", {
						 disabled: !vnode.attrs.room.has_someone_joined, 
						 value: currentTypedMessage, oninput: (e) => {currentTypedMessage = e.target.value}}),
					m("input.chat-submit[type='submit'][value='Invia']", {
						disabled: !vnode.attrs.room.has_someone_joined})
				])
				
			])
		}
	}
}