import {SiteHeaderTiny} from "./site-header-tiny.js"
import {auth} from "../model/auth.js"

export function Signup() {
	var errorMessage = ""
	
	var signup = function(e) {
		if (currentPassword != currentConfirmPassword) {
			errorMessage = "Le password inserite non combaciano"
		} else {
			m.request({
				method: "POST",
				url: "/SignupServlet",
				body: {username: currentUsername, password: currentPassword}
			}).then(function(result) {
				if (result.type === "signed-up") {
					auth.is_logged_in = true
					auth.username = currentUsername
					m.route.set("/home")
				} else if (result.type === "error") {
					if (result.content === "userAlreadyExists") {
						errorMessage = "Utente già esistente, scegliere un username diverso"
					}
					setTimeout((function() { errorMessage = ""; m.redraw() }), 6000)
				}
			})
		}
		e.preventDefault()
		return false
	}
	
	var currentUsername = "", currentPassword = "", currentConfirmPassword = ""
	
	return { 
		view: function() {
			return m("div.signup-container", [
				m(SiteHeaderTiny),
				m(m.route.Link, {href: "/home"}, "Torna alla Home"),
				m("h2.signup-title", "Registrati"),
				m("form", {onsubmit: (e) => signup(e)}, [
					m("input[type='text'][placeholder='username']",
						{oninput: (e) => {currentUsername = e.target.value}}),
					m("br"),
					m("input[type='password'][placeholder='password']",
						{oninput: (e) => {currentPassword = e.target.value}}),
					m("br"),
					m("input[type='password'][placeholder='conferma password']",
						{oninput: (e) => {currentConfirmPassword = e.target.value}}),
					m("br"),
					m("input[type='submit'][value='Registrati'].signup-button")
				]),
				m("p.error-message", errorMessage)
			])
		}
	}
}